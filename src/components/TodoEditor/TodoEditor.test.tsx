import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';

import { useDispatch } from 'react-redux';

import { EDIT_TODO, Todo } from '../../modules/TodosReducer';
import { TOGGLE_PAGE } from '../../modules/PageToggleReducer';

import TodoEditor from './TodoEditor';

import todos from '../../../fixtures/todos';


jest.mock('react-redux');

describe('<TodoEditor/>', () => {
  
  const setEditButtonSelected = jest.fn();
  const onSubmit = jest.fn();

  jest.spyOn(window, 'alert').mockImplementation(() => {});

  const renderTodoEditor = (
    text: string,
    title: string,
    selectedTodo: Todo,
    setEditButtonSelected: React.Dispatch<React.SetStateAction<boolean>>
  ) => {
    return render(
      <TodoEditor
        text={text}
        title={title}
        selectedTodo={selectedTodo}
        setEditButtonSelected={setEditButtonSelected}
      />
    );
  };

  it("Vérifier si une alerte s'affiche lorsque le contenu et la couleur ne sont pas spécifiés", () => {
    onSubmit.mockImplementation((event) => {
      event.preventDefault();
    });

    const dispatch = jest.fn();
    //@ts-ignore
    useDispatch.mockImplementation(() => dispatch);

    renderTodoEditor(
      'editer',
      'example title',
      todos[0],
      setEditButtonSelected
    );

    const todoInput = screen.getByRole('textbox');
    const editIcon = screen.getByTestId('submitBtn');
    fireEvent.change(todoInput, { target: { value: '' } });
    fireEvent.click(editIcon);
    
    expect(window.alert).toHaveBeenCalledWith('Veuillez spécifier le contenu et la couleur');

    //Error: Not implemented: HTMLFormElement.prototype.submit
  });

  it('EDIT_TODO action dispatch confirm', () => {
    const dispatch = jest.fn();
    //@ts-ignore
    useDispatch.mockImplementation(() => dispatch);

    renderTodoEditor(
      'editer',
      'example title',
      todos[0],
      setEditButtonSelected
    );

    const todoInput = screen.getByRole('textbox');
    const editIcon = screen.getByTestId('submitBtn');
    fireEvent.change(todoInput, {
      target: { value: 'todoContent Exemple 1 : Rouge -> Changer pour les bacs' },
    });
    fireEvent.click(editIcon);

    expect(dispatch).toBeCalledWith({
      type: EDIT_TODO,
      todo: {
        ...todos[0],
        todoContent: 'todoContent Exemple 1 : Rouge -> Changer pour les bacs',
      },
    });

    expect(dispatch).toBeCalledWith({
      type: TOGGLE_PAGE,
    });

    expect(setEditButtonSelected).toHaveBeenCalledTimes(1);
    //Error: Not implemented: HTMLFormElement.prototype.submit
  });

  it('ADD_TODO action dispatch confirm', () => {
    const dispatch = jest.fn();
    //@ts-ignore
    useDispatch.mockImplementation(() => dispatch);

    renderTodoEditor(
      'ajouter',
      'example title',
      { id: 0, todoContent: '', todoColor: '', done: false },
      setEditButtonSelected
    );

    const todoInput = screen.getByRole('textbox');
    const addIcon = screen.getByTestId('submitBtn');
    fireEvent.change(todoInput, {
      target: { value: 'todoContent Exemple 3 : Jaune' },
    });
    fireEvent.click(addIcon);

    expect(dispatch).toBeCalledTimes(2);
  });
});
