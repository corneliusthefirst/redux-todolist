/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React from 'react';
import styled from 'styled-components';

import GlobalStyles from './styles/GlobalStyles';
import TodoList from './components/TodoList/TodoList';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  margin: 0 auto;

  max-width: 575px;
  min-height: 612px;

  box-shadow: 0 0 1rem 0 rgba(0, 0, 0, 0.1);
`;

function App() {
  return (
  <div style={{marginTop: 50}}>
  <Wrapper>
      <TodoList />
      <GlobalStyles />
    </Wrapper>
  </div>
 

 
  );
}

export default App;
